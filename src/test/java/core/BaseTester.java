package core;
import org.junit.BeforeClass;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;



public class BaseTester implements Constantes {

	
	@BeforeClass
	public static void setup() {
		

		RestAssured.baseURI = URL_BASE;

		RestAssured.basePath = BASE_PAHT;
		
		RequestSpecBuilder reqBuilder = new RequestSpecBuilder();
		reqBuilder.setContentType(CONTENT_TYPE); 
		RestAssured.requestSpecification = reqBuilder.build();
		 	
		RestAssured.enableLoggingOfRequestAndResponseIfValidationFails();

		
	}

}