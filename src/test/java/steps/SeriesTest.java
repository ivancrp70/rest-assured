package steps;

import static io.restassured.RestAssured.given;

import org.hamcrest.Matchers;
import org.junit.Test;

import core.BaseTester;
import generica.util;
import io.qameta.allure.Description;
import io.qameta.allure.Story;

public class SeriesTest extends BaseTester {

	String TOKEN = util.auth();

	@Test
	@Story("Atenticação")
	@Description("Deve negar requisição sem auteticação")
	public void deveRetornarAcessoNegado401() {

		given().when().get("3/tv/119051").then().log().all().statusCode(401);

	}

	@Test
	@Story("Consultar Serie")
    @Description("Deve Retornar uma Serie")
	public void deveRetornarSerieWednesday() {

		given().header("Authorization", TOKEN).when().get("3/tv/119051").then().log().all().statusCode(200).body("name",
				Matchers.is("Wednesday"));

	}

	@Test
	@Story("Consultar Serie")
    @Description("Não requisitar serie sem o codigo ")
	public void naoDeveRetornarResultado() {

		given().header("Authorization", TOKEN).when().get("/3/tv/").then().log().all().statusCode(404)
				.body("status_message", Matchers.is("The resource you requested could not be found."));

	}

}
