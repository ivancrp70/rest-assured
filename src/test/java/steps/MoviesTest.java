package steps;

import static io.restassured.RestAssured.given;

import org.hamcrest.Matchers;
import org.junit.Test;

import core.BaseTester;
import generica.util;
import io.qameta.allure.Description;
import io.qameta.allure.Story;

public class MoviesTest extends BaseTester {

	String TOKEN = util.auth();

	@Test
	@Story("Atenticação")
	@Description("Deve negar requisição sem auteticação")
	public void deveRetornarAcessoNegado401() {

		 given().when().get("/3/movie/436270").then().log().all().statusCode(401);

	}

	@Test
	@Story("Consultar Filme")
    @Description("Deve Retornar uma Filme")
	public void deveRetornarFilmeBlackAdan() {

		given().header("Authorization", TOKEN).when().get("/3/movie/436270").then().log().all().statusCode(200)
				.body("original_title", Matchers.is("Black Adam"));

	}

	@Test
	@Story("Consultar Filme")
    @Description("Não requisitar Filme sem o codigo ")
	public void naoDeveRetornarResultado() {

		given().header("Authorization", TOKEN).when().get("/3/movie/").then().log().all().statusCode(404).body("status_message",
				Matchers.is("The resource you requested could not be found."));

	}

}
