package steps;

import static io.restassured.RestAssured.given;

import org.hamcrest.Matchers;
import org.junit.Test;

import core.BaseTester;
import generica.util;
import io.qameta.allure.Description;
import io.qameta.allure.Story;

public class PersonTest extends BaseTester {

	String TOKEN = util.auth();

	@Test
	@Story("Atenticação")
    @Description("Deve negar requisição sem auteticação")
	public void deveRetornarAcessoNegado401() {

		 given().when().get("/3/person/974169").then().log().all().statusCode(401);

	}

	@Test
	@Story("Consultar Pessoal")
    @Description("Deve Retornar uma Pessoal")
	public void deveRetornarPersonaJennaMarieOrtega() {

		given().header("Authorization", TOKEN).when().get("3/person/974169").then().log().all().statusCode(200).body("name",
				Matchers.is("Jenna Ortega"));

	}

	@Test
	@Story("Consultar Pessoal")
    @Description("Não requisitar pessoa sem o codigo ")
	public void naoDeveRetornarResultado() {

		given().header("Authorization", TOKEN).when().get("3/person/").then().log().all().statusCode(404).body("status_message",
				Matchers.is("The resource you requested could not be found."));

	}
}
