package trabalho.teste.software.suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite.SuiteClasses;

import steps.MoviesTest;
import steps.PersonTest;
import steps.SeriesTest;

@RunWith(org.junit.runners.Suite.class)
@SuiteClasses({ 
							MoviesTest.class,
							SeriesTest.class, 
							PersonTest.class 
})

public class RunnerTes {

}
